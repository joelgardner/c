#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    int value;
    struct node *link;
} node;

typedef struct list {
	node *head;
	int length;
} list;

void list_add(list *l, int value) {
	// traverse the list
	node **n = &(l->head);
	while (*n) 
		n = &(*n)->link;

	// create and hook up a new node
	*n = (node *)malloc(sizeof(node));
 	(*n)->value = value;
 	(*n)->link = NULL;

 	++l->length;
}

node* list_find(list *l, int value) {
	// traverse the list
	node **n = &(l->head);
	while (*n && (*n)->value != value) {
		n = &(*n)->link;
	}
	return *n;
}

void list_delete(list *l, int value) {

	// traverse the list
	node **n = &(l->head);
	while (*n && (*n)->link && (*n)->link->value != value) {
		n = &(*n)->link;
	}

	// delete node and hook up the last part of the list
	node *removed = (*n)->link;
	if (!removed) {
		printf("Did not find value %d in list.\n", value);
		return;
	}
	(*n)->link = removed->link;

	// tidy up
	free(removed);
	--l->length;
}

void list_print(list *l) {
	// traverse the list
	node **n = &(l->head);
	printf("{ ");
	while (*n) {
		printf("%d ", (*n)->value);
		n = &(*n)->link;
	}
	printf("}\n");
}

int main() {
	list *l = (list *)malloc(sizeof(list));

	list_add(l, 0);
	list_add(l, 1);
	list_add(l, 2);
	list_add(l, 3);
	list_add(l, 4);
	list_print(l);
	list_delete(l, 2);
	list_print(l);
	list_add(l, 2);
	list_print(l);
	list_delete(l, 2);
	list_print(l);
	list_delete(l, 14);
	list_add(l, 14);
	list_print(l);
	list_delete(l, 14);
	list_print(l);
	return 0;
}